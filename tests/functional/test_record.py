import os
import pytest
import re
import docker

from cli_test_helpers import ArgvContext, EnvironContext

import cogniceptshell.interface


def setup_file(tmpdir):
    """
    Utility function to setup a fake runtime.env to run tests
    """
    p = tmpdir.join("runtime.env")
    p.write("COG_AGENT_CONTAINERS=container1;container2\nCOG_AGENT_IMAGES=image1;image2")


def run_mock_bagger_server(tmpdir):
    """
    Utility function that runs mock docker container to test record commands
    """
    p = tmpdir.join("record_cmd.bash")
    commands = """#!/bin/bash
if [ "$1" == "start" ]; then
    echo "b'Recording state: Started\nGoal Succeeded\n'"
elif [ $1 == "pause" ]; then
    echo "b'Recording state: Paused\nGoal Succeeded\n'"
elif [ $1 == "resume" ]; then
    echo "b'Recording state: Resumed\nGoal Succeeded\n'"
elif [ $1 == "status" ]; then
    echo "b'Recording state: Resumed\nGoal Succeeded\n'"
elif [ $1 == "stop" ]; then
    echo "b'Recording state: Ready\nGoal Succeeded\n'"
else
    echo "Not Valid"
fi
"""
    p.write(commands)
    cmd_path = tmpdir.strpath + '/record_cmd.bash'
    os.chmod(cmd_path , 0o777)
    client = docker.from_env()
    options = {
        "name": "cgs_bagger_server",
        "detach": True,
        "auto_remove": True,
        "working_dir": "/test",
        "volumes": {tmpdir: {"bind": "/usr/local/bin", "mode": "rw"}}
    }

    client.containers.run("ubuntu:latest", "sleep 300", **options)


def stop_mock_bagger_server():
    """
    Utility function to stop and remove mock docker container
    """
    client = docker.from_env()
    container = client.containers.get("cgs_bagger_server")
    container.stop()


def check_mock_bagger_server_status():
    """
    Utility function to check mock docker container status
    """
    client = docker.from_env()

    try:
        client.containers.get("cgs_bagger_server")
        return True
    except:
        return False


def check_value(expected_pattern, output, expected_num_occurences):
    """
    Utility function to assert if an expected pattern is in an output 
    for specified # of occurrences
    """
    matches = re.findall(expected_pattern, output, re.MULTILINE)
    assert len(matches) == expected_num_occurences


def test_setup(tmpdir):
    """
    Test to see if the rest of the tests can be run.
    If a bagger server is already available, exit test to not 
    interfere with regular operations.
    """
    if(check_mock_bagger_server_status() is True):
        pytest.exit(
            """
            Skipping all tests since bagger server detected. 
            Stop existing servers so testing can continue with a mock server.
            """, 2)


def test_no_record_command(tmpdir, capsys):
    """
    Test if mandatory command missing is caught correctly
    """
    setup_file(tmpdir)
    with ArgvContext('cognicept', 'record', '--path', str(tmpdir) + "/"):
        try:
            cogniceptshell.interface.main()
            output = str(capsys.readouterr().out)
        except:
            pytest.fail(
                "Failed to check mandatory record command.", pytrace=True)

    expected_pattern = r"Required command is missing. Check `cognicept record --help` for more commands available."
    expected_num_occurences = 1
    check_value(expected_pattern, output, expected_num_occurences)


def test_no_start_args(tmpdir, capsys):
    """
    Test exception is thrown when start command does not have arguments
    """
    setup_file(tmpdir)

    with ArgvContext('cognicept', 'record', '--path', str(tmpdir) + "/", '--start'):
        try:
            cogniceptshell.interface.main()
            pytest.fail(
                "No exception thrown for no start command.", pytrace=True)

        except SystemExit:
            # Expecting a system exit
            pass

        except Exception:
            pytest.fail(
                "Failed to throw exception for no start args.", pytrace=True)


def test_no_server(tmpdir, capsys):
    """
    Test if bagger server is missing, it is caught correctly
    """
    setup_file(tmpdir)

    with ArgvContext('cognicept', 'record', '--path', str(tmpdir) + "/", '--start', 'rosout'):
        try:
            cogniceptshell.interface.main()
            output = str(capsys.readouterr().out)
        except:
            pytest.fail("Failed to check server is running.", pytrace=True)

    expected_pattern = r"DOCKER ERROR"
    expected_num_occurences = 1
    check_value(expected_pattern, output, expected_num_occurences)

    # Run mock bagger server for the rest of the tests
    run_mock_bagger_server(tmpdir)


def test_start(tmpdir, capsys):
    """
    Test if start command performs correctly
    """
    setup_file(tmpdir)

    with ArgvContext('cognicept', 'record', '--path', str(tmpdir) + "/", '--start', 'rosout'):
        try:
            cogniceptshell.interface.main()
            output = str(capsys.readouterr().out)
        except:
            pytest.fail("Uncaught exception detected", pytrace=True)

    expected_pattern = r"Started"
    expected_num_occurences = 1
    check_value(expected_pattern, output, expected_num_occurences)


def test_pause(tmpdir, capsys):
    """
    Test if pause command performs correctly
    """
    setup_file(tmpdir)

    with ArgvContext('cognicept', 'record', '--path', str(tmpdir) + "/", '--pause'):
        try:
            cogniceptshell.interface.main()
            output = str(capsys.readouterr().out)
        except:
            pytest.fail("Uncaught exception detected", pytrace=True)

    expected_pattern = r"Paused"
    expected_num_occurences = 1
    check_value(expected_pattern, output, expected_num_occurences)


def test_resume(tmpdir, capsys):
    """
    Test if resume command performs correctly
    """
    setup_file(tmpdir)

    with ArgvContext('cognicept', 'record', '--path', str(tmpdir) + "/", '--resume'):
        try:
            cogniceptshell.interface.main()
            output = str(capsys.readouterr().out)
        except:
            pytest.fail("Uncaught exception detected", pytrace=True)

    expected_pattern = r"Resumed"
    expected_num_occurences = 1
    check_value(expected_pattern, output, expected_num_occurences)


def test_status(tmpdir, capsys):
    """
    Test if status command performs correctly
    """
    setup_file(tmpdir)

    with ArgvContext('cognicept', 'record', '--path', str(tmpdir) + "/", '--status'):
        try:
            cogniceptshell.interface.main()
            output = str(capsys.readouterr().out)
        except:
            pytest.fail("Uncaught exception detected", pytrace=True)

    expected_pattern = r"Resumed"
    expected_num_occurences = 1
    check_value(expected_pattern, output, expected_num_occurences)


def test_stop(tmpdir, capsys):
    """
    Test if stop command performs correctly
    """
    setup_file(tmpdir)

    with ArgvContext('cognicept', 'record', '--path', str(tmpdir) + "/", '--stop'):
        try:
            cogniceptshell.interface.main()
            output = str(capsys.readouterr().out)
        except:
            pytest.fail("Uncaught exception detected", pytrace=True)

    expected_pattern = r"Ready"
    expected_num_occurences = 1
    check_value(expected_pattern, output, expected_num_occurences)

    # # Stop mock bagger server to finish tests
    # stop_mock_bagger_server()


def test_xclean():
    """
    Test to clean up that will run even if one or more of tests above fail.
    """
    if check_mock_bagger_server_status() is True:
        stop_mock_bagger_server()
